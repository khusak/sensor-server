package main

import (
	"net/http"

	"github.com/h00s/projekt1/sensors"
)

var led *sensors.LED

func Led(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("proba"))
}

func LedOn(w http.ResponseWriter, r *http.Request) {
	led.On()
}

func main() {
	led = sensors.NewLED()

	http.HandleFunc("/led", Led)
	http.HandleFunc("/led/on", LedOn)

	http.ListenAndServe(":1337", nil)
}
