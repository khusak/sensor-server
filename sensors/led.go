package sensors

import "time"

type LED struct {
	lastChange time.Time
}

func NewLED() *LED {
	return &LED{}
}

func (l *LED) On() {
	l.lastChange = time.Now()
}
